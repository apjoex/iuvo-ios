//
//  ForumTableViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 22/09/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import JGProgressHUD
import FirebaseDatabase

class ForumTableViewController: UITableViewController {
    
    var channels = [String]()
    var hud : JGProgressHUD!
    var currentUser : UserData!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let currentUser = RealmHelper.shared.getUser() {
            if !currentUser.isGeneralUser{
                navigationItem.rightBarButtonItem = nil
            }
        }
        
        currentUser = RealmHelper.shared.getUser()!
        getForums()
    }
    
    func getForums() {
        
        hud = showHUD(message: "Please wait...")
        
        let forumDbRef = Database.database().reference().child("users").child(currentUser.key!).child("forums")
        forumDbRef.observe(DataEventType.value) { (dataSnapshot : DataSnapshot) in
            self.hud.dismiss()
            if dataSnapshot.exists() {
                dataSnapshot.children.forEach({ (data) in
                    let snapShot = data as! DataSnapshot
                    self.channels.append(snapShot.key)
                })
                self.tableView.reloadData()
            }else{
                self.hud.dismiss()
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return channels.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ChatTableViewCell
        cell.configure(channels[indexPath.row])
        return cell
    }
 
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(channels[indexPath.row])
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "chatVC") as! ChatViewController
        viewController.title = channels[indexPath.row]
        viewController.isForum = true
        viewController.name = channels[indexPath.row]
        viewController.channelID = ""
        navigationController?.pushViewController(viewController, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
