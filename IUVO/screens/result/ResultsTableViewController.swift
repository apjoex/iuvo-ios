//
//  ResultsTableViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 14/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import CoreLocation
import JGProgressHUD
import Alamofire
import Pulley
import GeoFire
import FirebaseDatabase

class ResultsTableViewController: UITableViewController {
    
    var mItem : Item?
    let locationManager = CLLocationManager()
    
    var hud : JGProgressHUD!

    fileprivate var currentLat : Double = 0.0
    fileprivate var currentLong : Double = 0.0
    
    fileprivate var results = [Result]()
    var resultKeys = [String]()
    
    var dataEntered = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = mItem?.name ?? ""
        let backBtn = UIBarButtonItem()
        backBtn.title = ""
        navigationItem.backBarButtonItem = backBtn
    
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        getCurrentLocation()
    }
    
    func getCurrentLocation() {
        
        //Check if custom location is set
        
        if BaseUtils.shared.isCustomLocationSet {
            currentLat = BaseUtils.shared.customLat
            currentLong = BaseUtils.shared.customlong
            runQuery()
        }else{
            if CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied {
                hud = self.showHUD(message: "Getting current location...")
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            }else{
                showPromptAndFinish(message: "Please enable the location permission for IUVO via the iOS Settings app")
            }
        }
    }
    
    func runQuery() {
        
        if let mItem = mItem {
            
            hud = self.showHUD(message: "Please wait...")
            
            if mItem.isFromGoogle {
                //Get from Google places
                let url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?sensor=true&key=\(BaseUtils.shared.API_KEY)&type=\(mItem.type)&location=\(currentLat),\(currentLong)&radius=\(BaseUtils.shared.radius)"
                print(url)
                Alamofire.request(url, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (dataResponse) in
                    
                    guard dataResponse.result.isSuccess else {
                        self.showPromptAndFinish(message: dataResponse.result.error?.localizedDescription ?? BaseUtils.shared.defaultErrorMsg)
                        return
                    }
                    
                    self.results.removeAll()
                    
                    do {
                        let apiResponse = try JSONDecoder().decode(WebsiteData.self, from: dataResponse.data!)
                        //print(apiResponse)
                        
                        guard let mResults = apiResponse.results else { return }
                        
                        if mResults.count > 0 {
                            self.hud.dismiss()
                            self.results = mResults
                            self.tableView.reloadData()
                        }else{
                            self.hud.dismiss()
                            self.showPromptAndFinish(message: "No results found")
                        }
                    }catch{
                        print(error.localizedDescription)
                    }
                }
            }else{
                //Get from Firebase
                
                resultKeys.removeAll()
                
                let geoRef = Database.database().reference().child("geofire").child(BaseUtils.shared.makeFirebaseFriendlyPath(name: mItem.name))
                let geoFire = GeoFire(firebaseRef: geoRef)
                let origin = CLLocation(latitude: currentLat, longitude: currentLong)
                let resultQuery = geoFire.query(at: origin, withRadius: 30.6)
                
                resultQuery.observe(GFEventType.keyEntered) { (key, location) in
                    self.resultKeys.append(key)
                }
                
                resultQuery.observeReady {
                    self.resultKeys = self.resultKeys.removeDuplicates()
                    if self.resultKeys.count > 0 {
                        self.getResultsData()
                    }else{
                        self.hud.dismiss()
                        self.showPromptAndFinish(message: "No results found")
                    }
                }
            }
        }
    }
    
    func getResultsData() {
        
        let disptachgroup = DispatchGroup()
        
        resultKeys.forEach { (key) in
            disptachgroup.enter()
            Database.database().reference().child("businesses").child(BaseUtils.shared.makeFirebaseFriendlyPath(name: mItem?.name ?? "")).child(key).observeSingleEvent(of: DataEventType.value, with: { (dataSnapshot) in
                if dataSnapshot.exists() {
                    let snapShotDict = dataSnapshot.value as! [String : Any]
                    let mResult = Result(dict: snapShotDict)
                    self.results.append(mResult)
                    disptachgroup.leave()
                }
            })
            
            
        }
        
        disptachgroup.notify(queue: DispatchQueue.main) {
            //All work finished
            if self.results.count  > 0 {
                self.hud.dismiss()
                self.tableView.reloadData()
            }else{
                self.showPromptAndFinish(message: "No results found")
            }
        }
        
    }
    
  

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return results.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ResultTableViewCell
        cell.configure(place: results[indexPath.row], item : mItem!, currentLocation : CLLocation(latitude: currentLat, longitude: currentLong))
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        DataUtils.shared.mPlace = results[indexPath.item]
        DataUtils.shared.mItem = mItem!
        
        let distance = CLLocation(latitude: currentLat, longitude: currentLong).distance(from: CLLocation(latitude: (results[indexPath.item].geometry?.location?.lat)!, longitude: (results[indexPath.item].geometry?.location?.lng)!)) / 1000
        DataUtils.shared.mDistance = distance

        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mapView") as! PulleyViewController
        viewController.title = results[indexPath.item].name
        navigationController?.pushViewController(viewController, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: false)
    }

}

extension ResultsTableViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else {
            hud.dismiss()
            return
        }
        
        currentLat = locValue.latitude
        currentLong = locValue.longitude
        locationManager.stopUpdatingLocation()
        locationManager.delegate = nil
        
        hud.dismiss()
        runQuery()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.denied) {
            showPrompt(message: "Please enable the location permission for IUVO via the iOS Settings app")
        }else{
            //   getCurrentLocation()
        }
    }

    
}
