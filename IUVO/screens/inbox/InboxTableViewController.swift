//
//  InboxTableViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 22/09/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import JGProgressHUD
import FirebaseDatabase

class InboxTableViewController: UITableViewController {

    var channelIDs = [String]()
    var names = [String]()
    var hud : JGProgressHUD!
    var currentUser : UserData!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let currentUser = RealmHelper.shared.getUser() {
            if !currentUser.isGeneralUser{
                navigationItem.rightBarButtonItem = nil
            }
        }
        
        currentUser = RealmHelper.shared.getUser()!
        getChannels()
    }
    
    func getChannels() {
        hud = showHUD(message: "Please wait...")
        let channelDbRef = Database.database().reference().child("users").child(currentUser.key!).child("chatChannels")
        channelDbRef.observe(DataEventType.value) { (dataSnapshot : DataSnapshot) in
            if dataSnapshot.exists() {
                dataSnapshot.children.forEach({ (data) in
                    let snapShot = data as! DataSnapshot
                    self.channelIDs.append(snapShot.key)
                    self.names.append(snapShot.value as! String)
                })
                self.hud.dismiss()
                self.tableView.reloadData()
            }else{
                self.hud.dismiss()
                self.showPrompt(message: "You have no inbox messages")
            }
        }
    }


    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return channelIDs.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ChatTableViewCell
        cell.configure(names[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "chatVC") as! ChatViewController
        viewController.title = names[indexPath.row]
        viewController.isForum = false
        viewController.name = names[indexPath.row]
        viewController.channelID = channelIDs[indexPath.row]
        navigationController?.pushViewController(viewController, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
