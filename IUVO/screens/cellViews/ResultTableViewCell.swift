//
//  ResultTableViewCell.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 29/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import CoreLocation


class ResultTableViewCell: UITableViewCell {
    
    @IBOutlet weak var itemImgBg: UIView!
    @IBOutlet weak var itemImg: UIImageView!
    @IBOutlet weak var placeRatingLabel: UILabel!
    @IBOutlet weak var placeTitleLabel: UILabel!
    @IBOutlet weak var placeVicinityLabel: UILabel!
    @IBOutlet weak var placeStatusLabel: UILabel!
    @IBOutlet weak var distaceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(place: Result, item : Item?, currentLocation : CLLocation?) {
        
        if let item = item {
            itemImg.image = item.img
        }
        
        if let ratings = place.rating {
            if ratings > Float(0){
                placeRatingLabel.text = "\(ratings)★"
            }
        }
        
        placeTitleLabel.text = place.name
        placeVicinityLabel.text = place.vicinity
        
        if place.opening_hours?.open_now == true {
            placeStatusLabel.text = "OPEN"
            placeStatusLabel.textColor = UIColor(named: "colorGreen")
        }else{
            placeStatusLabel.text = "CLOSED"
            placeStatusLabel.textColor = UIColor(named: "colorRed")
        }
        
        if let currentLocation = currentLocation {
            let distance = currentLocation.distance(from: CLLocation(latitude: (place.geometry?.location?.lat)!, longitude: (place.geometry?.location?.lng)!)) / 1000
            distaceLabel.text = (String(format: "%.01f km away", distance))
        }else{
            distaceLabel.text = ""
        }
        
    }


    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        itemImgBg.backgroundColor = UIColor(named: "colorPrimary")

        // Configure the view for the selected state
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        itemImgBg.backgroundColor = UIColor(named: "colorPrimary")
    }

}
