//
//  ChatTableViewCell.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 22/09/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    @IBOutlet weak var chatName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(_ name : String) {
        chatName.text = name
    }

}
