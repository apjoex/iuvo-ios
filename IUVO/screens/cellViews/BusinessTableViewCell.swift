//
//  BusinessTableViewCell.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 03/10/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit

class BusinessTableViewCell: UITableViewCell {

    @IBOutlet weak var iconBg: UIView!
    @IBOutlet weak var businessName: UILabel!
    @IBOutlet weak var businessAddress: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(_ business : Result) {
        businessName.text = business.name
        businessAddress.text = business.vicinity
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        iconBg.backgroundColor = UIColor(named: "colorPrimary")
    }
    

}
