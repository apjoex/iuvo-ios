//
//  BusinessOwnerViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 06/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import IQDropDownTextField
import FirebaseAuth
import FirebaseDatabase
import CropViewController
import FirebaseStorage

class BusinessOwnerViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var meansOfID: IQDropDownTextField!
    @IBOutlet weak var idNumberTextField: UITextField!
    @IBOutlet weak var tcLabel: UILabel!
    
    var imagePicker = UIImagePickerController()
    var newUserData = UserData()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        


        meansOfID.placeholder = "Means of Identification"
        meansOfID.isOptionalDropDown = true
        meansOfID.itemList = ["International passport", "National ID Card", "Driver license"]
        
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: tcLabel.text ?? "")
        attributedString.setColor(color: UIColor(named: "colorPrimary")!, forText: "Terms and Conditions")
        tcLabel.attributedText = attributedString
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapTerms))
        tcLabel.addGestureRecognizer(tap)
    }
    
    @IBAction func didTapSignUpButton(_ sender: Any) {
        
        if validateField().status {
            //Create User
            let hud = showHUD(message: "Signing Up...")
            let auth = Auth.auth()
            auth.createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (result, error) in
                if let error = error {
                    //Error occured
                    hud.dismiss()
                    self.showPrompt(message: error.localizedDescription)
                    return
                }
                
                //Create User data
                let newUserUID = result!.user.uid
                
                self.newUserData.key = newUserUID
                self.newUserData.firstName = self.firstNameTextField.text!
                self.newUserData.lastName = self.lastNameTextField.text!
                self.newUserData.fullNmae = "\(self.firstNameTextField.text!) \(self.lastNameTextField.text!)"
                self.newUserData.email = self.emailTextField.text!
                self.newUserData.phone = self.phoneTextField.text!
                self.newUserData.isGeneralUser = false
                self.newUserData.verified = false
                self.newUserData.meansOfID = self.meansOfID.selectedItem!
                self.newUserData.idNumber = self.idNumberTextField.text!
                self.newUserData.needsUpload = true
                
                let dbRef = Database.database().reference()
                dbRef.child("users").child(newUserUID).setValue(self.newUserData.toDict(), withCompletionBlock: { (error, ref) in
                    if let error = error {
                        hud.dismiss()
                        self.showPrompt(message: error.localizedDescription)
                        return
                    }
                    
                    hud.dismiss()
                    let alert = UIAlertController(title: "One more step", message: "You need to upload a clear picture of your means of ID for verification. \nOnce this is verified successfully, you can proceed to add your businesses", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Choose Photo", style: UIAlertAction.Style.default, handler: { (_) in
                        //Choose Photo
                        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                            print("Button capture")
                            self.imagePicker.delegate = self
                            self.imagePicker.sourceType = .savedPhotosAlbum
                            self.imagePicker.allowsEditing = false
                            self.present(self.imagePicker, animated: true, completion: nil)
                        }
                    }))
                    alert.addAction(UIAlertAction(title: "Take Photo", style: UIAlertAction.Style.default, handler: { (_) in
                        //Take photo
                        if UIImagePickerController.isSourceTypeAvailable(.camera){
                            print("Button capture")
                            self.imagePicker.delegate = self
                            self.imagePicker.sourceType = .camera
                            self.imagePicker.allowsEditing = false
                            self.present(self.imagePicker, animated: true, completion: nil)
                        }
                    }))
                    self.present(alert, animated: true, completion: nil)
                    
                })
            }
            
        }else{
            showPrompt(message: validateField().message)
        }
    }
    
    func validateField() -> (status: Bool, message: String) {
        
        if firstNameTextField.text!.isEmpty {
            return (false, "Please enter first name")
        }
        
        if lastNameTextField.text!.isEmpty {
            return (false, "Please enter last name")
        }
        
        if !emailTextField.text!.isValidEmail {
            return (false, "Please enter a valid email address")
        }
        
        if phoneTextField.text!.count < 11 {
            return (false, "Please enter a valid phone number")
        }
        
        if passwordTextField.text!.count < 8 {
            return (false, "Password must have a minimum of 8 charaters")
        }
        
        if passwordTextField.text != confirmPasswordTextField.text {
            return (false, "Passwords do not match")
        }
        
        if meansOfID.selectedRow == -1 {
            return (false, "Please select a valid means of identification")
        }
        
        if idNumberTextField.text!.isEmpty {
            return (false, "Please enter your ID number")
        }
        
        return (true, "")
    }
    
    @objc func didTapTerms() {
        
        if let url = URL(string: "http://www.iuvohub.com/terms.html"){
            
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil )
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            DispatchQueue.main.async {
                let cropViewController = CropViewController(image: pickedImage)
                cropViewController.delegate = self
                self.present(cropViewController, animated: true, completion: nil)
            }
        }
        dismiss(animated: true, completion: nil)
        
    }
    
    func goHome() {
        RealmHelper.shared.create(newUserData)
        BaseUtils.shared.isLoggedIn = true
        self.performSegue(withIdentifier: "proceedHomeBO", sender: nil)
    }
    
    func uploadImageForVefification(_ image: UIImage) {
        let hud = showHUD(message: "Uploading image for verification...")
        let data = image.jpegData(compressionQuality: 0.8)
        let filePath = "verification_iamges/\(newUserData.key!).jpg"
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"
        let storageRef = Storage.storage().reference().child(filePath)
        storageRef.putData(data!, metadata: metaData){(metaData,error) in
            hud.dismiss()
            if error != nil {
                Database.database().reference().child("users").child(self.newUserData.key!).child("declineMessage").setValue("Please upload your ID")
                self.showPromptAndTakeAction(message: error?.localizedDescription ?? "Image upload was NOT successful", action: self.goHome)
            }else{
                storageRef.downloadURL(completion: { (url, _) in
                    print("Download URL is \(url?.absoluteString ?? "")")
                    
                    let newVerificationkey = Database.database().reference().child("verification_requests").childByAutoId().key!
                    
                    let userDbRef = Database.database().reference().child("users").child(self.newUserData.key!)
                    userDbRef.child("needsUpload").setValue(false)
                    userDbRef.child("declineMessage").setValue("Your ID is awaiting verification...")
                    
                    self.newUserData.needsUpload = false
                    self.newUserData.declineMessage = "Your ID is awaiting verification..."
                    
                    let verificationRequestDbRef = Database.database().reference().child("verification_requests").child(newVerificationkey)
                    verificationRequestDbRef.child("userName").setValue(self.newUserData.fullNmae)
                    verificationRequestDbRef.child("userID").setValue(self.newUserData.key)
                    verificationRequestDbRef.child("imageURL").setValue(url?.absoluteString )

                    self.showPromptAndTakeAction(message: error?.localizedDescription ?? "Image upload was successful", action: self.goHome)

                })
            }
        }
    }
}

extension BusinessOwnerViewController : CropViewControllerDelegate {
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        cropViewController.dismiss(animated: true, completion: nil)
        self.uploadImageForVefification(image)
    }
}
