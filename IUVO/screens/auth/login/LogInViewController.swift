//
//  LogInViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 06/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase
import JGProgressHUD

class LogInViewController: UIViewController {
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    let minLength = BaseUtils.shared.MINIMUM_CHARACTER_LENGTH
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    @IBAction func signInBtnTapped(_ sender: Any) {
        if validateFields().status {
            //proceed to auth
            signIn()
        }else{
            showPrompt(message: validateFields().message)
        }
    }
    
    func validateFields() -> (status : Bool, message : String) {
        
        if !(emailField.text ?? "" ).isValidEmail {
            return (false, "Please provide a valid email address")
        }
        
        if (passwordField.text ?? "" ).count < minLength {
            return (false, "Password must have a minimum of \(minLength) characters")
        }
        
        
        return (true , "")
    }
    
    func signIn() {
        let hud = self.showHUD(message: "Signing in...")
        Auth.auth().signIn(withEmail: emailField.text!, password: passwordField.text!) { (mAuthResult, mError) in
            
            guard let result = mAuthResult else {
                hud.dismiss()
                self.showPrompt(message: mError?.localizedDescription ?? BaseUtils.shared.defaultErrorMsg)
                return
            }
            
            //Sucessful login
          
            if result.user.isEmailVerified || result.user.uid == "0VevZk1hGIYQVKlPsgW70QUNsrw2" || result.user.uid == "ETmDNJ6v4cbsauUNvYhtjumeO5P2"{
                //User is verified
                self.getUserData(result.user.uid, hud: hud)
            }else{
                hud.dismiss()
                let alert = UIAlertController(title: "Verify Email Address", message: "Please click the link in the mail sent to \(result.user.email ?? "") to verify your email address", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Resend link", style: UIAlertAction.Style.default, handler: { (_) in
                    result.user.sendEmailVerification(completion: nil)
                }))
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.cancel, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    func getUserData(_ uid : String, hud : JGProgressHUD) {
        Database.database().reference().child("users").child(uid).observeSingleEvent(of: DataEventType.value) { (dataSnapShot) in
            if dataSnapShot.exists() {
                let mData = dataSnapShot.value as! [String : Any]
                let userData = BaseUtils.shared.toUserDataClass(data: mData)
                RealmHelper.shared.create(userData)
                BaseUtils.shared.isLoggedIn = true
                hud.dismiss()
                self.performSegue(withIdentifier: "goHome", sender: nil)
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
