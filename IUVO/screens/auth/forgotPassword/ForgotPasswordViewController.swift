//
//  ForgotPasswordViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 06/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import FirebaseAuth

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var emailAddressField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func resetBtnTapped(_ sender: Any) {
        if (emailAddressField.text?.isValidEmail)! {
            resetPassword(forEmail: emailAddressField.text!)
        }else{
            showPrompt(message: "Please provide a valid email address")
        }
    }
    
    func resetPassword(forEmail email : String) {
        
        let hud = showHUD(message: "Resetting password...")
        
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            
            hud.dismiss()
            self.emailAddressField.text = ""
            
            guard error == nil else {
                self.showPrompt(message: error?.localizedDescription ?? BaseUtils.shared.defaultErrorMsg)
                return
            }
            
            self.showPrompt(message: "Check your email to reset password.")
            
        }
    }
}
