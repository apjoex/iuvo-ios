//
//  SignUpViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 06/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func businessOwnerBtnTapped(_ sender: Any) {
        performSegue(withIdentifier: "proceedToBusinessOwner", sender: nil)
    }
    
    @IBAction func generalUserBtnTapped(_ sender: Any) {
        performSegue(withIdentifier: "proceedToGeneralUser", sender: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
