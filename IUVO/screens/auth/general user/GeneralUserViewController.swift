//
//  GeneralUserViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 06/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class GeneralUserViewController: UIViewController {

    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var tcLabel: UILabel!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: tcLabel.text ?? "")
        attributedString.setColor(color: UIColor(named: "colorPrimary")!, forText: "Terms and Conditions")
        tcLabel.attributedText = attributedString
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(didTapTerms))
        tcLabel.addGestureRecognizer(tap)
        
    }
    
    @IBAction func didTapSignUpButton(_ sender: Any) {
        if validateField().status {
            //Create User
            let hud = showHUD(message: "Signing Up...")
            let auth = Auth.auth()
            auth.createUser(withEmail: emailTextField.text!, password: passwordTextField.text!) { (result, error) in
                if let error = error {
                    //Error occured
                    hud.dismiss()
                    self.showPrompt(message: error.localizedDescription)
                    return
                }
                
                //Create User data
                let newUserUID = result!.user.uid
                
                let newUserData = UserData()
                newUserData.key = newUserUID
                newUserData.firstName = self.firstNameTextField.text!
                newUserData.lastName = self.lastNameTextField.text!
                newUserData.fullNmae = "\(self.firstNameTextField.text!) \(self.lastNameTextField.text!)"
                newUserData.email = self.emailTextField.text!
                newUserData.phone = self.phoneTextField.text!
                newUserData.isGeneralUser = true
                
                let dbRef = Database.database().reference()
                dbRef.child("users").child(newUserUID).setValue(newUserData.toDict(), withCompletionBlock: { (error, ref) in
                    if let error = error {
                        hud.dismiss()
                        self.showPrompt(message: error.localizedDescription)
                        return
                    }
                    
                    RealmHelper.shared.create(newUserData)
                    BaseUtils.shared.isLoggedIn = true
                    hud.dismiss()
                    self.performSegue(withIdentifier: "proceedHomeGU", sender: nil)
                    
                })
            }
            
        }else{
            showPrompt(message: validateField().message)
        }
    }
    
    
    func validateField() -> (status: Bool, message: String) {
        
        if firstNameTextField.text!.isEmpty {
            return (false, "Please enter first name")
        }
        
        if lastNameTextField.text!.isEmpty {
            return (false, "Please enter last name")
        }
        
        if !emailTextField.text!.isValidEmail {
            return (false, "Please enter a valid email address")
        }
        
        if phoneTextField.text!.count < 11 {
            return (false, "Please enter a valid phone number")
        }
        
        if passwordTextField.text!.count < 8 {
            return (false, "Password must have a minimum of 8 charaters")
        }
        
        if passwordTextField.text != confirmPasswordTextField.text {
            return (false, "Passwords do not match")
        }
        
        return (true, "")
    }
    
    @objc func didTapTerms() {
        
        if let url = URL(string: "http://www.iuvohub.com/terms.html"){

            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil )
            } else {
                UIApplication.shared.openURL(url)
            }
        }
        
    }

}
