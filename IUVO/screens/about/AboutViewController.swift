//
//  AboutViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 30/09/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if let currentUser = RealmHelper.shared.getUser() {
            if !currentUser.isGeneralUser{
                navigationItem.rightBarButtonItem = nil
            }
        }
    }

}
