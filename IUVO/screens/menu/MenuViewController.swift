//
//  MenuViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 09/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import KYDrawerController
import SDWebImage

class MenuViewController: UIViewController {

    @IBOutlet weak var profileAvatar: UIImageView!
    @IBOutlet weak var profileName: UILabel!
    
    @IBOutlet weak var profileBox: UIView!
    @IBOutlet weak var favBox: UIView!
    @IBOutlet weak var settingsBox: UIView!
    @IBOutlet weak var aboutBox: UIView!
    @IBOutlet weak var forumBox: UIView!
    @IBOutlet weak var inboxBox: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let currentUser = RealmHelper.shared.getUser()
        
        profileName.text = currentUser?.fullNmae ?? "-"
        //profileAvatar.sd_setImage(with: URL(string: currentUser?.profilePic ?? ""), completed: nil)
        
        if let currentUser = currentUser {
            if currentUser.isGeneralUser {
                forumBox.isHidden = true
            }else{
                
                favBox.isHidden = true
                settingsBox.isHidden = true
                
                //Is a business owner
                if currentUser.verified {
                    //Get businesses
                }else{
                    forumBox.isHidden = true
                    inboxBox.isHidden = true
                    profileBox.isHidden = true
                }
                
                
            }
        }
    }
    
    @IBAction func profileBtnTapped(_ sender: Any) {
        if let drawerController = self.parent as? KYDrawerController.DrawerController {
            drawerController.setDrawerState(DrawerController.DrawerState.closed, animated: true)
            let navVC = drawerController.mainViewController as! UINavigationController
            let homeVC = navVC.viewControllers.first as! HomeViewController
            homeVC.performSegue(withIdentifier: "goToProfile", sender: nil)
        }
    }
    
    @IBAction func favBtnTapped(_ sender: Any) {
        if let drawerController = self.parent as? KYDrawerController.DrawerController {
            drawerController.setDrawerState(DrawerController.DrawerState.closed, animated: true)
            let navVC = drawerController.mainViewController as! UINavigationController
            let homeVC = navVC.viewControllers.first as! HomeViewController
            homeVC.performSegue(withIdentifier: "goToFavourites", sender: nil)
        }
    }
    
    @IBAction func settingsBtnTapped(_ sender: Any) {
        if let drawerController = self.parent as? KYDrawerController.DrawerController {
            drawerController.setDrawerState(DrawerController.DrawerState.closed, animated: true)
            let navVC = drawerController.mainViewController as! UINavigationController
            let homeVC = navVC.viewControllers.first as! HomeViewController
            homeVC.performSegue(withIdentifier: "goToSettings", sender: nil)
        }
    }
    
    @IBAction func aboutBtnTapped(_ sender: Any) {
        if let drawerController = self.parent as? KYDrawerController.DrawerController {
            drawerController.setDrawerState(DrawerController.DrawerState.closed, animated: true)
            let navVC = drawerController.mainViewController as! UINavigationController
            let homeVC = navVC.viewControllers.first as! HomeViewController
            homeVC.performSegue(withIdentifier: "goToAbout", sender: nil)
        }
    }
    
    @IBAction func forumBtnTapped(_ sender: Any) {
        if let drawerController = self.parent as? KYDrawerController.DrawerController {
            drawerController.setDrawerState(DrawerController.DrawerState.closed, animated: true)
            let navVC = drawerController.mainViewController as! UINavigationController
            let homeVC = navVC.viewControllers.first as! HomeViewController
            homeVC.performSegue(withIdentifier: "goToForum", sender: nil)
        }
    }
    
    @IBAction func inboxBtnTapped(_ sender: Any) {
        if let drawerController = self.parent as? KYDrawerController.DrawerController {
            drawerController.setDrawerState(DrawerController.DrawerState.closed, animated: true)
            let navVC = drawerController.mainViewController as! UINavigationController
            let homeVC = navVC.viewControllers.first as! HomeViewController
            homeVC.performSegue(withIdentifier: "goToInbox", sender: nil)
        }
    }
    
    @IBAction func logOutBtnTapped(_ sender: Any) {
        if let drawerController = self.parent as? KYDrawerController.DrawerController {
            drawerController.setDrawerState(DrawerController.DrawerState.closed, animated: true)
            let navVC = drawerController.mainViewController as! UINavigationController
            let homeVC = navVC.viewControllers.first as! HomeViewController
            homeVC.promptLogOut()
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
