//
//  ChatViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 23/09/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import FirebaseDatabase
import MessageKit
import MessageUI

class ChatViewController: MessagesViewController {
    
    let currentUser = RealmHelper.shared.getUser()
    
    var isForum : Bool!
    var name : String!
    var channelID : String!
    
    private var messages: [Message] = []
    var messeagesRef : DatabaseReference!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "•••", style: UIBarButtonItem.Style.plain, target: self, action: #selector(menuTapped))

        messageInputBar.delegate = self
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        
        scrollsToBottomOnKeybordBeginsEditing = true
        
        if isForum {
            let forumChannelID = BaseUtils.shared.makeFirebaseFriendlyPath(name: name)
            messeagesRef = Database.database().reference().child("forumChannels").child(forumChannelID).child("messages")
        }else{
            messeagesRef = Database.database().reference().child("chatChannels").child(channelID).child("messages")
        }
        
        //Set up for child added
        messeagesRef.observe(.childAdded) { (data) in
            if data.exists() {
                let dataDict = data.value as! [String : Any]
                let mMessage = Message(sender: Sender(id: dataDict["senderID"] as! String, displayName: dataDict["senderName"] as? String ?? ""), messageId: data.key, sentDate: Date(timeIntervalSince1970: dataDict["time"] as! TimeInterval), kind: MessageKind.text(dataDict["text"] as! String))
                self.messages.append(mMessage)
                self.messagesCollectionView.reloadData()
                self.messagesCollectionView.scrollToBottom(animated: true)
            }
        }
        
    }
    
    @objc func menuTapped (){
        let sheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        sheet.addAction(UIAlertAction(title: "Report abuse", style: UIAlertAction.Style.default, handler: { (_) in
            if !MFMailComposeViewController.canSendMail() {
                let alert = UIAlertController(title: "Mail services not available", message: "Please send a mail to \ncomplaints@iuvohub.com", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Okay", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
                return
            }else{
                let composeVC = MFMailComposeViewController()
                composeVC.mailComposeDelegate = self
                composeVC.setToRecipients(["complaints@iuvohub.com"])
                composeVC.setSubject("IUVO: Abuse Report")
                composeVC.setMessageBody("\n\nfrom \(self.currentUser?.fullNmae! ?? "")", isHTML: false)
                self.present(composeVC, animated: true, completion: nil)
            }
            
        }))
        sheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
            sheet.dismiss(animated: true, completion: nil)
        }))
        present(sheet, animated: true, completion: nil)
    }
    
    func isFromCurrentSender(message : Message) -> Bool {
        return message.sender.id == currentUser!.key!
    }

}

extension ChatViewController: MessagesDisplayDelegate {
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        avatarView.initials = String(message.sender.displayName.prefix(1))
    }
    
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return isFromCurrentSender(message: message) ? UIColor(named: "colorPrimary")! : .gray
    }
    
    func textColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        return .white
    }
    
    func shouldDisplayHeader(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> Bool {
        return true
    }
    
    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        let corner: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
        return .bubbleTail(corner, .curved)
    }
    
}

extension ChatViewController: MessagesDataSource {
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
    
    func currentSender() -> Sender {
        return Sender(id: currentUser!.key!, displayName: currentUser!.fullNmae!)
    }
    
    func numberOfMessages(in messagesCollectionView: MessagesCollectionView) -> Int {
        return messages.count
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return messages[indexPath.section]
    }
    
    func cellTopLabelAttributedText(for message: MessageType, at indexPath: IndexPath) -> NSAttributedString? {
        let name = message.sender.displayName
        return NSAttributedString(
            string: name,
            attributes: [
                .foregroundColor: UIColor.black
            ]
        )
    }
}

extension ChatViewController: MessagesLayoutDelegate {
    
    func avatarSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
        return .zero
    }
    
    func footerViewSize(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> CGSize {
        return CGSize(width: 0, height: 0)
    }
    
    func heightForLocation(message: MessageType, at indexPath: IndexPath, with maxWidth: CGFloat, in messagesCollectionView: MessagesCollectionView) -> CGFloat {
        return 0
    }
}

extension ChatViewController: MessageInputBarDelegate {
    
    func messageInputBar(_ inputBar: MessageInputBar, didPressSendButtonWith text: String) {
        
        
        let msgDict : [String : Any] = ["senderID" : currentUser?.key ?? "",
                                        "senderName" : currentUser?.fullNmae ?? "",
                                        "text" : text,
                                        "time" : (Date().timeIntervalSince1970 * 1000).rounded()]
        
        messeagesRef.childByAutoId().setValue(msgDict) { (error, dbRef) in
            if error != nil {
                self.showPrompt(message: "Your message could not be sent. Please try again")
            }else{
                inputBar.inputTextView.text = ""
                
                if !self.isForum {
                    //Send PN for private messages
                    let participants = self.channelID.split(separator: ",")
                    let otherUser = participants.filter({ (s) -> Bool in
                        s != self.currentUser!.key!
                    }).first
                    
                    Database.database().reference().child("users").child("\(otherUser!)").child("fcmToken").observeSingleEvent(of: DataEventType.value, with: { (dataSnapshot) in
                        if dataSnapshot.exists() {
                            let otherUserFCM = dataSnapshot.value as! String
                            
                            //Send notifications
                            let notifData : [String : Any] = ["message": text,
                                                              "receiverFCM" : otherUserFCM,
                                                              "senderName": self.currentUser?.fullNmae ?? ""]
                            Database.database().reference().child("notificationsList").childByAutoId().setValue(notifData)
                        }
                    })
                }
                
            }
        }
        
        inputBar.inputTextView.text = String()
        messagesCollectionView.scrollToBottom()
    }
    
}

extension ChatViewController: MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        if let error = error {
            print(error)
        }
        controller.dismiss(animated: true, completion: nil)
    }
}
