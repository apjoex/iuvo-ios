//
//  SettingsViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 09/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import GooglePlaces
import GooglePlacePicker

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var locationbtn: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if BaseUtils.shared.isCustomLocationSet {
            self.locationbtn.setTitle(BaseUtils.shared.customAddress, for: UIControl.State.normal)
        }
    }

    
    @IBAction func locationBtnTapped(_ sender: Any) {
        let options = UIAlertController(title: "Set location", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        
        let currentLocationOption = UIAlertAction(title: "Use current location always", style: UIAlertAction.Style.default) { (_) in
            BaseUtils.shared.isCustomLocationSet = false
            self.locationbtn.setTitle("Use current location always", for: UIControl.State.normal)
        }
        
        let customLocationOption = UIAlertAction(title: "Choose custom location", style: UIAlertAction.Style.default) { (_) in
            self.promptGooglePlaces()
        }
        
        let cancelOption = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (_) in
            
        }
        
        options.addAction(currentLocationOption)
        options.addAction(customLocationOption)
        options.addAction(cancelOption)
        
        present(options, animated: true, completion: nil)
    }
    
    func promptGooglePlaces() {
        let autocompleteController = GMSAutocompleteViewController()
        let filter = GMSAutocompleteFilter()
        filter.country = "NG"
        autocompleteController.autocompleteFilter = filter
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }

}

extension SettingsViewController : GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        let newLat = place.coordinate.latitude as Double
        let newLong = place.coordinate.longitude as Double
        
        BaseUtils.shared.isCustomLocationSet = true
        BaseUtils.shared.customLat = newLat
        BaseUtils.shared.customlong = newLong
        BaseUtils.shared.customAddress = place.formattedAddress ?? ""
        
        locationbtn.setTitle(place.formattedAddress ?? "", for: UIControl.State.normal)

        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print(error)
        dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
