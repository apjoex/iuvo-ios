//
//  ItemCollectionViewCell.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 13/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var itemImg: UIImageView!
    @IBOutlet weak var itemLabel: UILabel!
    
    
}
