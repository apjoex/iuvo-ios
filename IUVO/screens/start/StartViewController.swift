//
//  StartViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 06/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit

class StartViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        //Check if onBoarding has been shown
        if BaseUtils.shared.onBoardingShown {
            if BaseUtils.shared.isLoggedIn {
                proceedTo(segueIdentifier : "proceedToHome")
            }else{
                proceedTo(segueIdentifier: "proceedToLogIn")
            }
        }else{
            proceedTo(segueIdentifier: "proceedToOnboarding")
        }
    }
    
    func proceedTo(segueIdentifier : String) {
        performSegue(withIdentifier: segueIdentifier, sender: nil)
    }


}
