//
//  HomeViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 07/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import KYDrawerController
import FirebaseDatabase

class HomeViewController: UIViewController {
    
    var currentUser : UserData?
    
    @IBOutlet weak var servicesBox: UIView!
    @IBOutlet weak var repairsBox: UIView!
    @IBOutlet weak var salesBox: UIView!
    @IBOutlet weak var verificationBox: UIView!
    @IBOutlet weak var declineMsgLabel: UILabel!
    @IBOutlet weak var uploadBtn: UIButton!
    @IBOutlet weak var businessTableView: UITableView!
    @IBOutlet weak var businessBox: UIView!
    
    var businesses : [Result] = []
    var businessCount = 0
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentUser = RealmHelper.shared.getUser()
        
        let logo = #imageLiteral(resourceName: "iuvo_text_logo")
        let imageView = UIImageView(image: logo)
        navigationItem.titleView = imageView
        
        if let currentUser = currentUser {
            if currentUser.isGeneralUser{
                //General user is logged in
                verificationBox.isHidden = true
                businessBox.isHidden = true
            }else{
                //Business owner is logged in
                navigationItem.rightBarButtonItem = nil
                
                servicesBox.isHidden = true
                repairsBox.isHidden = true
                salesBox.isHidden = true
                
                if currentUser.verified {
                    //Show business owner's businesses
                    verificationBox.isHidden = true
                    businessBox.isHidden = false
                    fetchBusiness(currentUser)
                    
                    let rControl = UIRefreshControl()
                    rControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
                    businessTableView.refreshControl = rControl
                    
                }else{
                    //Show pending verification box
                    uploadBtn.isHidden = !currentUser.needsUpload
                    declineMsgLabel.text = currentUser.declineMessage ?? ""
                    verificationBox.isHidden = false
                    businessBox.isHidden = true

                }
                
            }
        }
    }
    
    @objc func refresh() {
        fetchBusiness(currentUser!)
    }
    
    func fetchBusiness(_ currentUser : UserData) {
        
        businessTableView.dataSource = self
        businessTableView.delegate = self
        
        let hud = showHUD(message: "One moment please...")
        
        Database.database().reference().child("users").child(currentUser.key ?? "").child("businesses").observe(DataEventType.value) { (dataSnapShot) in
             hud.dismiss()
            if dataSnapShot.exists() {
                self.businesses.removeAll()
                self.businessCount = Int(dataSnapShot.childrenCount)
                dataSnapShot.children.forEach({ (snapShot) in
                    let businessDataSnapshot = snapShot as! DataSnapshot
                    self.getBusinessData(businessDataSnapshot.key)
                })
            }else{
                self.businessTableView.refreshControl?.endRefreshing()
                self.showPrompt(message: "You are yet to register a business.")
            }
        }
        
    }
    
    func getBusinessData(_ key : String) {
        let goodPath = key.replacingOccurrences(of: "***", with: "/")
        Database.database().reference().child("businesses").child(goodPath).observe(DataEventType.value) { (dataSnapshot) in
            if dataSnapshot.exists() {
                let dataDict = dataSnapshot.value as! [String : Any]
                let mResult = Result(dict: dataDict)
                self.businesses.append(mResult)
                if self.businesses.count == self.businessCount {
                    self.businessTableView.refreshControl?.endRefreshing()
                    self.businessTableView.reloadData()
                }
            }
        }
    }
    
    func promptLogOut() {
        let alert = UIAlertController(title: "Log out", message: "Are you sure you want to log out?", preferredStyle: UIAlertController.Style.alert)
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.destructive) { (_) in
            self.logOutAndWipeData()
        }
        let noAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil)
        alert.addAction(noAction)
        alert.addAction(yesAction)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func uploadBtnTapped(_ sender: Any) {
       let sheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        sheet.addAction(UIAlertAction(title: "Choose photo", style: .default, handler: { (_) in
            
        }))
        sheet.addAction(UIAlertAction(title: "Take photo", style: .default, handler: { (_) in
            
        }))
        sheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
            sheet.dismiss(animated: true, completion: nil)
        }))
        present(sheet, animated: true, completion: nil)
    }
    
    @IBAction func menuBtnTapped(_ sender: Any) {
        if let drawerController = navigationController?.parent as? KYDrawerController.DrawerController {
            drawerController.setDrawerState(DrawerController.DrawerState.opened, animated: true)
        }
    }
    
    @IBAction func servicesTapped(_ sender: Any) {
        performSegue(withIdentifier: "goToServices", sender: nil)
    }
    
    @IBAction func repairsTapped(_ sender: Any) {
        performSegue(withIdentifier: "goToRepairs", sender: nil)
    }
    
    @IBAction func salesTapped(_ sender: Any) {
        performSegue(withIdentifier: "goToSales", sender: nil)
    }
}

extension HomeViewController : UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
}

extension HomeViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return businesses.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "businessCell") as! BusinessTableViewCell
        cell.configure(businesses[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.businessTableView.deselectRow(at: indexPath, animated: true)
    }
    
}
