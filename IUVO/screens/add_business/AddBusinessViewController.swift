//
//  AddBusinessViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 04/11/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import IQDropDownTextField
import GooglePlaces
import GooglePlacePicker
import FirebaseDatabase
import GeoFire

class AddBusinessViewController: UIViewController {

    @IBOutlet weak var natureField: IQDropDownTextField!
    @IBOutlet weak var categoriesField: IQDropDownTextField!
    @IBOutlet weak var businessNameTextField: UITextField!
    @IBOutlet weak var businessPhoneTextField: UITextField!
    @IBOutlet weak var businessEmailTextField: UITextField!
    @IBOutlet weak var businessWebsiteTextField: UITextField!
    @IBOutlet weak var businessAddressTextField: UITextField!
    
    var newBusinessDict = [String : Any]()
    
    var currentUser : UserData?
    var bizLat = 0.0
    var bizLng = 0.0
    
    var categories = [String](){
        didSet {
            categoriesField.itemList = categories
            categoriesField.setSelectedRow(0, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        natureField.isOptionalDropDown = true
        natureField.itemList = ["Services","Repairs","Sales"]
        natureField.delegate = self
        
        currentUser = RealmHelper.shared.getUser()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(promptGooglePlaces))
        tap.numberOfTapsRequired = 1
        businessAddressTextField.addGestureRecognizer(tap)

    }
    
    @objc func promptGooglePlaces() {
        let autocompleteController = GMSAutocompleteViewController()
        let filter = GMSAutocompleteFilter()
        filter.country = "NG"
        autocompleteController.autocompleteFilter = filter
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    @IBAction func didTapAddBusinessButton(_ sender: Any) {
        if validateField().status {
            let hud = showHUD(message: "Uploading business details")
            let fbPath = BaseUtils.shared.makeFirebaseFriendlyPath(name: categoriesField.selectedItem!)
            let newKey = Database.database().reference().child("businesses").child(fbPath).childByAutoId().key!
            
            newBusinessDict["id"] = newKey
            newBusinessDict["icon"] = ""
            newBusinessDict["name"] = businessNameTextField.text!
            newBusinessDict["place_id"] = newKey
            newBusinessDict["reference"] = newKey
            newBusinessDict["mail"] = businessEmailTextField.text!
            newBusinessDict["phone"] = businessPhoneTextField.text!
            newBusinessDict["website"] = businessWebsiteTextField.text!
            newBusinessDict["customSource"] = true
            newBusinessDict["ownerID"] = currentUser?.key ?? ""
            newBusinessDict["ownerName"] = currentUser?.fullNmae ?? ""
            newBusinessDict["category"] = fbPath
            
            let geoRef = Database.database().reference().child("geofire").child(fbPath)
            let geoFire = GeoFire(firebaseRef: geoRef)
            geoFire.setLocation(CLLocation(latitude: bizLat, longitude: bizLng), forKey: newKey)
            
            Database.database().reference().child("businesses").child(fbPath).child(newKey).setValue(newBusinessDict) { (error, ref) in
                if let _ = error {
                    hud.dismiss()
                    self.showPrompt(message: "Unable to upload business details. Please try again")
                    return
                }
                
                //Register for forum
                Database.database().reference().child("users").child(self.currentUser?.key ?? "").child("forums").child(self.categoriesField.selectedItem!).setValue(true)
                Database.database().reference().child("users").child(self.currentUser?.key ?? "").child("businesses").child("\(fbPath)***\(newKey)").setValue(true, withCompletionBlock: { (err, dbRef) in
                    if let _ = err {
                        hud.dismiss()
                        self.showPrompt(message: "Unable to link business")
                    }
                    
                    hud.dismiss()
                    self.showPromptAndFinish(message: "Business details uploaded successfully")
                    
                })
                
            }
            
        }else{
            showPrompt(message: validateField().message)
        }
    }
    
    func validateField() -> (status: Bool, message: String) {
        
        if natureField.selectedRow == -1 {
            return (false, "Please select a nature of business")
        }
        
        if categoriesField.selectedRow == -1 {
            return (false, "Please select a business category")
        }
        
        if businessNameTextField.text!.isEmpty {
            return (false, "Please enter a business name")
        }
        
        if !businessEmailTextField.text!.isValidEmail {
            return (false, "Please enter a valid email address")
        }
        
        if businessPhoneTextField.text!.count < 11 {
            return (false, "Please enter a valid phone number")
        }
        
        if businessAddressTextField.text!.isEmpty {
            return (false, "Please enter a business address")
        }
        
        return (true, "")
    }
    
}

extension AddBusinessViewController : IQDropDownTextFieldDelegate {
    func textField(_ textField: IQDropDownTextField, didSelectItem item: String?) {
        switch item {
        case "Services":
            categories = DataUtils.shared.services.filter({ (mItem) -> Bool in
                !mItem.isFromGoogle
            }).map({ (item) in
                item.name
            })
        case "Repairs":
            categories = DataUtils.shared.repairs.map({ (item) in
                item.name
            })
        case "Sales":
            categories = DataUtils.shared.sales.map({ (item) in
                item.name
            })
        default:
            categories.removeAll()
        }
    }
}

extension AddBusinessViewController : GMSAutocompleteViewControllerDelegate {
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        bizLat = place.coordinate.latitude
        bizLng = place.coordinate.longitude
        
        let dataDict = ["lat" : bizLat,
                        "lng" : bizLng]
        let geoDict = ["location" : dataDict]
        
        newBusinessDict["geometry"] = geoDict
        newBusinessDict["vicinity"] = place.formattedAddress ?? ""
        
        businessAddressTextField.text = place.formattedAddress ?? ""
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print(error)
        dismiss(animated: true, completion: nil)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}
