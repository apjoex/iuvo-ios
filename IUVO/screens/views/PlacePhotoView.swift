//
//  PlacePhotoView.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 01/11/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import SDWebImage

class PlacePhotoView: UIView {
    
    @IBOutlet weak var photoView: UIImageView!
    
    func configure (with placePhoto : Photo?) {
        let photoUrl = "https://maps.googleapis.com/maps/api/place/photo?photoreference=\(placePhoto?.photo_reference ?? "")&sensor=false&maxheight=300&maxwidth=300&key=\(BaseUtils.shared.API_KEY)"
        photoView.sd_setImage(with: URL(string: photoUrl), placeholderImage: UIImage(named: "logo_colored"))
    }
    

}
