//
//  ProfileViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 18/09/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import CropViewController
import FirebaseStorage
import FirebaseDatabase
import JGProgressHUD
import SDWebImage

class ProfileViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate  {
    
    var currentUser : UserData?
    
    @IBOutlet weak var avatarView: UIImageView!
    @IBOutlet weak var changeAvatarBtn: UIButton!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var lastNameField: UITextField!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var phoneNumberField: UITextField!
    @IBOutlet weak var actionBtn: UIButton!
    
    var newImage : UIImage!
    var newImageSet : Bool!
    
    var imagePicker = UIImagePickerController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        newImageSet = false

        if let mUser = RealmHelper.shared.getUser() {
            currentUser = mUser
            updateUI()
            if !mUser.isGeneralUser{
                navigationItem.rightBarButtonItem = nil
            }
        }
        
        enableFields(false)
        avatarView.clipsToBounds = true
    }

    @IBAction func didTapActionBtn(_ sender: Any) {
        if actionBtn.title(for: UIControl.State.normal)!.contains("Edit") {
            //Edit profile
            enableFields(true)
            changeAvatarBtn.isHidden = false
            emailLabel.isHidden = true
            emailField.isHidden = true
            actionBtn.setTitle("Save profile", for: UIControl.State.normal)
        }else{
            //Save Profile
            if(validateFields().satus){
                if newImageSet {
                     uploadImagePic(img: newImage, path: "avatar/\(currentUser?.key ?? "fallback").jpg")
                }else{
                    let post = ["firstName": firstNameField.text!,
                                "lastName": lastNameField.text!,
                                "fullNmae": "\(firstNameField.text!) \(lastNameField.text!)",
                                "phone": phoneNumberField.text!]
                    proceedToUpdateValues(post)
                }
            }else{
                showPrompt(message: validateFields().message)
            }
        }
    }
    
    func validateFields() -> (satus: Bool, message: String) {
        
        if (firstNameField.text ?? "").isEmpty {
            return (false, "Please enter first name")
        }
        
        if (lastNameField.text ?? "").isEmpty {
            return (false, "Please enter last name")
        }
        
        if (phoneNumberField.text?.count ?? 0) < 11 {
            return (false, "Please enter a valid phone number")
        }
        
        return (true , "")
    }
  
    func proceedToUpdateValues(_ updates : [String : String]) {
        Database.database().reference().child("users").child(currentUser?.key ?? "").updateChildValues(updates) { (error, dbRef) in
            if error != nil {
                //Error occurred
            }else{
                self.enableFields(false)
                self.changeAvatarBtn.isHidden = true
                self.emailLabel.isHidden = false
                self.emailField.isHidden = false
                self.actionBtn.setTitle("Edit profile", for: UIControl.State.normal)
                
                try! RealmHelper.shared.realm.write {
                    self.currentUser?.firstName = self.firstNameField.text!
                    self.currentUser?.lastName = self.lastNameField.text!
                    self.currentUser?.fullNmae = "\(self.firstNameField.text!) \(self.lastNameField.text!)"
                    self.currentUser?.phone = self.phoneNumberField.text!
                    if self.newImageSet { self.currentUser?.profilePic = updates["profilePic"]}
                }
                
                self.updateUI()
            }
            
            self.newImageSet = false
        }
    }
    
    @IBAction func didTapChangeAvatarBtn(_ sender: Any) {
        let sheet = UIAlertController(title: "Change profile picture", message: nil, preferredStyle: UIAlertController.Style.actionSheet)
        sheet.addAction(UIAlertAction(title: "Choose photo", style: .default, handler: { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
                print("Button capture")                
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .savedPhotosAlbum
                self.imagePicker.allowsEditing = false
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        sheet.addAction(UIAlertAction(title: "Take photo", style: .default, handler: { (_) in
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                print("Button capture")
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = .camera
                self.imagePicker.allowsEditing = false
                
                self.present(self.imagePicker, animated: true, completion: nil)
            }
        }))
        sheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (_) in
            sheet.dismiss(animated: true, completion: nil)
        }))
        present(sheet, animated: true, completion: nil)
    }
    
    func updateUI() {
        firstNameField.text = currentUser?.firstName ?? ""
        lastNameField.text = currentUser?.lastName ?? ""
        emailField.text = currentUser?.email ?? ""
        phoneNumberField.text = currentUser?.phone ?? ""
        avatarView.sd_setImage(with: URL(string: currentUser?.profilePic ?? ""), placeholderImage: UIImage(named: "user"))
    }
    
    func enableFields(_ status : Bool) {
        firstNameField.isUserInteractionEnabled = status
        lastNameField.isUserInteractionEnabled = status
        emailField.isUserInteractionEnabled = status
        phoneNumberField.isUserInteractionEnabled = status

    }

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            DispatchQueue.main.async {
                let cropViewController = CropViewController(image: pickedImage)
                cropViewController.delegate = self
                self.present(cropViewController, animated: true, completion: nil)
            }
        }
        dismiss(animated: true, completion: nil)

    }
    
    func uploadImagePic(img: UIImage, path: String) {
        let hud = showHUD(message: "Uploading avatar...")
        let data = img.jpegData(compressionQuality: 0.8)
        let filePath = "\(path)"
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"
        let storageRef = Storage.storage().reference().child(filePath)
        storageRef.putData(data!, metadata: metaData){(metaData,error) in
            hud.dismiss()
            if error != nil {
                self.showPrompt(message: error?.localizedDescription ?? "Something went wrong")
            }else{
                storageRef.downloadURL(completion: { (url, _) in
                    print("Download URL is \(url?.absoluteString ?? "")")
                    let post = ["firstName": self.firstNameField.text!,
                                "lastName": self.lastNameField.text!,
                                "fullNmae": "\(self.firstNameField.text!) \(self.lastNameField.text!)",
                                "phone": self.phoneNumberField.text!,
                                "profilePic" : url?.absoluteString ?? ""]
                    self.proceedToUpdateValues(post)
                })
            }
        }
    }
}

extension ProfileViewController : CropViewControllerDelegate {
    func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        // 'image' is the newly cropped version of the original image
        cropViewController.dismiss(animated: true, completion: nil)
        newImage = image
        avatarView.image = image
        newImageSet = true
    }
}
