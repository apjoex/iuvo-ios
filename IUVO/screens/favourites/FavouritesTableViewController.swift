//
//  FavouritesTableViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 22/09/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import JGProgressHUD
import FirebaseDatabase
import Alamofire
import Pulley

class FavouritesTableViewController: UITableViewController {
    
    var keysCount = 0
    var favPlaces = [Result]()
    
    var currentUser : UserData?
    var favDbRef : DatabaseReference!
    
    var hud : JGProgressHUD!
    
    let favItem : Item = Item(name: "", type: "", img: UIImage(named: "fav")!, isFromGoogle: true)

    override func viewDidLoad() {
        super.viewDidLoad()
       // self.tableView.register(ResultTableViewCell.self, forCellReuseIdentifier: "cell")
        currentUser = RealmHelper.shared.getUser()
        favDbRef = Database.database().reference().child("favourites").child(currentUser!.key!)
        getUserFavs()
    }
    
    func getUserFavs() {
        
        hud = showHUD(message: "Please wait")
        
        favDbRef.observe(DataEventType.value) { (dataSnapshot) in
            if dataSnapshot.exists() {
                self.keysCount = Int(dataSnapshot.childrenCount)
                dataSnapshot.children.forEach({ (snapShot) in
                    let it = snapShot as! DataSnapshot
                    if (it.value as! Bool) {
                        self.getGooglePlace(it.key)
                    }else{
                        self.getCustomPlace(it.key)
                    }
                })
            }else{
                self.hud.dismiss()
                self.showPrompt(message: "You have no favourites")
            }
        }
    }
    
    func getGooglePlace(_ placeID : String) {
        
        let placeUrl = "https://maps.googleapis.com/maps/api/place/details/json?placeid=\(placeID)&key=\(BaseUtils.shared.API_KEY)"
        Alamofire.request(placeUrl, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (dataResponse) in
            
            guard dataResponse.result.isSuccess else {
                self.showPromptAndFinish(message: dataResponse.result.error?.localizedDescription ?? BaseUtils.shared.defaultErrorMsg)
                return
            }
            
            do {
                let apiResponse = try JSONDecoder().decode(FavData.self, from: dataResponse.data!)
                guard let mResult = apiResponse.result else { return }
                self.favPlaces.append(mResult)
                if self.favPlaces.count == self.keysCount {
                    self.hud.dismiss()
                    self.tableView.reloadData()
                }
            }catch{
                print(error.localizedDescription)
            }
        }
    }
    
    func getCustomPlace(_ placeID : String) {
        let goodPath = placeID.replacingOccurrences(of: "***", with: "/")
        Database.database().reference().child("businesses").child(goodPath).observeSingleEvent(of: DataEventType.value) { (data) in
            if data.exists() {
                let snapShotDict = data.value as! [String : Any]
                let mResult = Result(dict: snapShotDict)
                self.favPlaces.append(mResult)
                if self.favPlaces.count == self.keysCount {
                    self.hud.dismiss()
                    self.tableView.reloadData()
                }
            }
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favPlaces.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ResultTableViewCell
        cell.configure(place: favPlaces[indexPath.row], item : favItem, currentLocation : nil)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        DataUtils.shared.mPlace = favPlaces[indexPath.item]
        DataUtils.shared.mItem = favItem
        
        DataUtils.shared.mDistance = 0.0
        
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mapView") as! PulleyViewController
        viewController.title = favPlaces[indexPath.item].name
        navigationController?.pushViewController(viewController, animated: true)
        
        tableView.deselectRow(at: indexPath, animated: false)
    }
    

}
