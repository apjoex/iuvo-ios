//
//  MapViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 29/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {

    let regionRadius: CLLocationDistance = 500
    @IBOutlet weak var mapView: MKMapView!
    
    var place = DataUtils.shared.mPlace
    
    override func viewDidLoad() {
        super.viewDidLoad()
        centerMapOnLocation(location: CLLocation(latitude: (place?.geometry?.location?.lat)!, longitude: (place?.geometry?.location?.lng)!))
        // Do any additional setup after loading the view.
    }
    
   
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegion.init(center: location.coordinate, latitudinalMeters: regionRadius, longitudinalMeters: regionRadius)
        mapView.setRegion(coordinateRegion, animated: true)
        let marker = Artwork(name: place!.name, vicinity: place!.vicinity, coordinate: CLLocationCoordinate2D(latitude: (place?.geometry?.location?.lat)!, longitude: (place?.geometry?.location?.lng)!))
        mapView.addAnnotation(marker)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

class Artwork: NSObject, MKAnnotation {
    let name: String?
    let vicinity: String
    let coordinate: CLLocationCoordinate2D
    
    init(name: String, vicinity: String, coordinate: CLLocationCoordinate2D) {
        self.name = name
        self.vicinity = vicinity
        self.coordinate = coordinate
        
        super.init()
    }
    
    var subtitle: String? {
        return vicinity
    }
}
