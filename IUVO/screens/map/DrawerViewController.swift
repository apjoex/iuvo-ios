//
//  DrawerViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 19/09/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit
import Pulley
import MessageUI
import FirebaseDatabase
import MapKit
import CarouselSwift
import SDWebImage

class DrawerViewController: UIViewController {

    @IBOutlet weak var itemImg: UIImageView!
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var placeAddress: UILabel!
    @IBOutlet weak var placeRating: UILabel!
    @IBOutlet weak var placeDistance: UILabel!
    @IBOutlet weak var carouselView: CarouselView!
    @IBOutlet weak var favBtn: UIButton!
    @IBOutlet weak var contactBox: UIStackView!
    @IBOutlet weak var chatImg: UIImageView!
    @IBOutlet weak var websiteImg: UIImageView!
    @IBOutlet weak var mailImg: UIImageView!
    @IBOutlet weak var phoneImg: UIImageView!
    
    var place = DataUtils.shared.mPlace
    
    var currentUser : UserData!
    
    var favDBRef : DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        carouselView.delegate = self
        carouselView.dataSource = self
        carouselView.cellPerPage = 3
        
        placeName.text = place?.name
        placeAddress.text = place?.vicinity
        itemImg.image = DataUtils.shared.mItem.img
        
        if DataUtils.shared.mDistance != 0.0 {
            placeDistance.text = (String(format: "%.01f km away", DataUtils.shared.mDistance))
        }else{
            placeDistance.text = ""
        }
        
        carouselView.reload()
        
        if let ratings = place?.rating {
            if ratings > Float(0){
                placeRating.text = "\(ratings)★"
            }
        }
        
        if let sUser = RealmHelper.shared.getUser() {
            currentUser = sUser
        }

        favDBRef = Database.database().reference().child("favourites").child(currentUser.key!)
        
        checkForFavStatus()
        
        if let place = place {
            contactBox.isHidden = !(place.customSource ?? false == true)
            websiteImg.isHidden = (place.website ?? "").isEmpty
            phoneImg.isHidden = (place.phone ?? "").isEmpty
            mailImg.isHidden = (place.mail ?? "").isEmpty
        }
        
    }
    
    func checkForFavStatus() {
        if let place = place {
            var path = ""
            if place.customSource ?? false {
                path = "\(place.category!)***\(place.place_id)"
            } else {
                path = place.place_id
            }
            
            favDBRef.child(path).observe(DataEventType.value) { (snapShot) in
                if snapShot.exists() {
                    self.favBtn.setTitle("REMOVE FROM FAVOURITES", for: UIControl.State.normal)
                }else{
                    self.favBtn.setTitle("ADD TO FAVOURITES", for: UIControl.State.normal)
                }
            }
        }
    }
    
    @IBAction func didTapFavBtn(_ sender: Any) {
        if (favBtn.title(for: UIControl.State.normal)?.contains("ADD"))! {
            self.addToFavourite(self.place)
        }else if (favBtn.title(for: UIControl.State.normal)?.contains("REMOVE"))!{
            self.removeFromFavourite(self.place)
        }
    }
    
    func addToFavourite(_ place : Result?) {
        if let place = place {
            var path = ""
            if place.customSource ?? false {
                path = "\(place.category!)***\(place.place_id)"
                favDBRef.child(path).setValue(false ) { (error, ref) in
                    if error != nil {
                        //There was an error
                        self.showPrompt(message: error?.localizedDescription ?? "There was an unexpected error. Please try again")
                    }else{
                        //No error
                        self.favBtn.setTitle("REMOVE FROM FAVOURITES", for: UIControl.State.normal)
                    }
                    
                }
            } else {
                path = place.place_id
                favDBRef.child(path).setValue(true) { (error, ref) in
                    if error != nil {
                        //There was an error
                        self.showPrompt(message: error?.localizedDescription ?? "There was an unexpected error. Please try again")
                    }else{
                        //No error
                        self.favBtn.setTitle("REMOVE FROM FAVOURITES", for: UIControl.State.normal)
                    }
                    
                }
            }
        }
    }
    
    func removeFromFavourite(_ place : Result?) {
        if let place = place {
            var path = ""
            if place.customSource ?? false {
                path = "\(place.category!)***\(place.place_id)"
            } else {
                path = place.place_id
            }
            
            favDBRef.child(path).setValue(nil) { (error, ref) in
                if error != nil {
                    //There was an error
                    self.showPrompt(message: error?.localizedDescription ?? "There was an unexpected error. Please try again")
                }else{
                    //No error
                    self.favBtn.setTitle("ADD TO FAVOURITES", for: UIControl.State.normal)
                }
                
            }
        }
    }
    
    @IBAction func didTapChatBtn(_ sender: Any) {
        let actionAlert = UIAlertController(title: "Chat", message: "Do you want to start chatting with the business owner?", preferredStyle: UIAlertController.Style.alert)
        actionAlert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: { (_) in
            
        }))
        actionAlert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (_) in
            //Start chat
            let members = [self.place?.ownerID ?? "", self.currentUser.key ?? ""].sorted()
            let channelID = members.joined(separator: ", ")
            
            //Attach chat to business owner
            Database.database().reference().child("users").child(self.place?.ownerID ?? "").child("chatChannels").child(channelID).setValue(self.currentUser.fullNmae)
            
            //Attach chat to general user
            Database.database().reference().child("users").child(self.currentUser.key ?? "").child("chatChannels").child(channelID).setValue(self.place?.ownerName ?? "")
            
            let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "chatVC") as! ChatViewController
            viewController.title =  self.place?.ownerName ?? ""
            viewController.isForum = false
            viewController.name = self.place?.ownerName ?? ""
            viewController.channelID = channelID
            self.navigationController?.pushViewController(viewController, animated: true)

            
        }))
        present(actionAlert, animated: true, completion: nil)
    }
    
    @IBAction func didTapWebsiteBtn(_ sender: Any) {
        let actionAlert = UIAlertController(title: "Visit website", message: "Do you want to visit the business website?", preferredStyle: UIAlertController.Style.alert)
        actionAlert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: { (_) in
            
        }))
        actionAlert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (_) in
            
            if let link = URL(string: "https://\(self.place?.website ?? "")") {
                UIApplication.shared.open(link)
            }
            
        }))
        present(actionAlert, animated: true, completion: nil)
    }
    
    @IBAction func didTapMailBtn(_ sender: Any) {
        let actionAlert = UIAlertController(title: "Send Mail", message: "Do you want to mail the business owner?", preferredStyle: UIAlertController.Style.alert)
        actionAlert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: { (_) in
            
        }))
        actionAlert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (_) in
            
            if !MFMailComposeViewController.canSendMail() {
                self.showPrompt(message: "Send a mail to \n\(self.place?.mail ?? "")")
                print("Mail services are not available")
                return
            }
            
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            
            // Configure the fields of the interface.
            composeVC.setToRecipients([self.place?.mail ?? ""])
            composeVC.setSubject("IUVO: Mail from customer")
            composeVC.setMessageBody("", isHTML: false)
            
            // Present the view controller modally.
            self.present(composeVC, animated: true, completion: nil)
            
        }))
        present(actionAlert, animated: true, completion: nil)
    }
    
    @IBAction func didTapCallBtn(_ sender: Any) {
        let actionAlert = UIAlertController(title: "Call", message: "Do you want to call the business owner?", preferredStyle: UIAlertController.Style.alert)
        actionAlert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: { (_) in
    
        }))
        actionAlert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (_) in
            let number = self.place?.phone ?? ""
            let url : NSURL = URL(string: "TEL://\(number)")! as NSURL
            UIApplication.shared.open(url as URL, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: nil)
        }))
        present(actionAlert, animated: true, completion: nil)
    }
    
    @IBAction func navigationBtnTapped(_ sender: Any) {
        let coordinate = CLLocationCoordinate2DMake(place?.geometry?.location?.lat ?? 0.0, place?.geometry?.location?.lng ?? 0.0)
        
//        let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: coordinate, addressDictionary:nil))
//        mapItem.name = "Destination/Target Address or Name"
//        mapItem.openInMaps(launchOptions: [MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving])
        
        self.present(PazNavigationApp.directionsAlertController(coordinate: coordinate), animated: true, completion: nil) 
        
    }
    
    
}

extension DrawerViewController : PulleyDrawerViewControllerDelegate {
    
    func partialRevealDrawerHeight(bottomSafeArea: CGFloat) -> CGFloat {
        return 280
    }
    
}

extension DrawerViewController : MFMailComposeViewControllerDelegate{
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        // Check the result or perform other tasks.
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
}

extension DrawerViewController : CarouselViewDelegate, CarouselViewDataSourse {
    
    func carousel(_ carousel: CarouselView, viewForIndex index: Int) -> UIView? {
        let view = (Bundle.main.loadNibNamed("PlacePhotoView", owner: self, options: nil)?[0] as? PlacePhotoView)!
        view.configure(with: place?.photos?[index])
        return view
    }
    
    func carousel(_ carousel: CarouselView, didTapAt cell: Int) {
        let photoUrl = "https://maps.googleapis.com/maps/api/place/photo?photoreference=\(place?.photos?[cell].photo_reference ?? "")&sensor=false&maxheight=300&maxwidth=300&key=\(BaseUtils.shared.API_KEY)"
        let fullSCreenImg = UIImageView(frame: view.frame)
        fullSCreenImg.contentMode = UIView.ContentMode.scaleAspectFit
        fullSCreenImg.backgroundColor = UIColor.black
        fullSCreenImg.tag = 100
        fullSCreenImg.sd_setImage(with: URL(string: photoUrl), placeholderImage: UIImage(named: "logo_colored"))
        fullSCreenImg.isUserInteractionEnabled = true
        let dismissTap = UITapGestureRecognizer(target: self, action: #selector(removeImage))
        dismissTap.numberOfTapsRequired = 1
        fullSCreenImg.addGestureRecognizer(dismissTap)
        
        self.view.addSubview(fullSCreenImg)

    }
    
    func numberOfView(_ carousel: CarouselView) -> Int {
        return place?.photos?.count ?? 0
    }
    
    @objc func removeImage(){
        let imageView = self.view.viewWithTag(100) as! UIImageView
        imageView.removeFromSuperview()
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
