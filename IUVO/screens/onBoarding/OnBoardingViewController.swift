//
//  OnBoardingViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 15/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit

class OnBoardingViewController: UIPageViewController, UIPageViewControllerDataSource {
    

    override func viewDidLoad() {
        super.viewDidLoad()

        dataSource = self
        self.view.backgroundColor = UIColor.white
        
        let proxy = UIPageControl.appearance()
        proxy.pageIndicatorTintColor = UIColor.gray
        proxy.currentPageIndicatorTintColor = UIColor(named: "colorPrimary")
        
        let pageOne = storyboard!.instantiateViewController(withIdentifier: "pageOne") as! PageOneViewController
        
        setViewControllers([pageOne], direction: UIPageViewController.NavigationDirection.forward, animated: true, completion: nil)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        let pageOne = storyboard!.instantiateViewController(withIdentifier: "pageOne") as! PageOneViewController
        let pageTwo = storyboard!.instantiateViewController(withIdentifier: "pageTwo") as! PageTwoViewController
    
        if viewController.isKind(of: PageThreeViewController.self) {
            return pageTwo
        }else if viewController.isKind(of: PageTwoViewController.self){
            return pageOne
        }else {
            return nil
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        let pageTwo = storyboard!.instantiateViewController(withIdentifier: "pageTwo") as! PageTwoViewController
        let pageThree = storyboard!.instantiateViewController(withIdentifier: "pageThree") as! PageThreeViewController
        
        if viewController.isKind(of: PageOneViewController.self){
            return pageTwo
        }else if viewController.isKind(of: PageTwoViewController.self){
            return pageThree
        }else{
            return nil
        }
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return 3
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }

}
