//
//  PageThreeViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 15/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit

class PageThreeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func getStartedBtnTapped(_ sender: Any) {
        BaseUtils.shared.onBoardingShown = true
        performSegue(withIdentifier: "logIn", sender: nil)
    }
    

}
