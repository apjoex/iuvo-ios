//
//  SearchCollectionViewController.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 15/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit


class SearchCollectionViewController: UICollectionViewController,  UISearchResultsUpdating {

    let searchController = UISearchController(searchResultsController: nil)
    
    var filteredItems = [Item]()
    var allItems = [Item]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        allItems.append(contentsOf: DataUtils.shared.services)
        allItems.append(contentsOf: DataUtils.shared.repairs)
        allItems.append(contentsOf: DataUtils.shared.sales)

        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.systemFont(ofSize: 14)
        UILabel.appearance(whenContainedInInstancesOf: [UISearchBar.self]).font = UIFont.systemFont(ofSize: 14)
        
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.isActive = true
        searchController.searchBar.placeholder = "Find any business right now"
        searchController.searchBar.isTranslucent = false
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        definesPresentationContext = true

        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false


        // Do any additional setup after loading the view.
    }
    
    // MARK: Search controller methods
    func updateSearchResults(for searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text ?? "")
    }

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredItems.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ItemCollectionViewCell
        
        // Configure the cell
        cell.itemImg.image = filteredItems[indexPath.item].img
        cell.itemLabel.text = filteredItems[indexPath.item].name
    
        return cell
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredItems = allItems.filter({
            return $0.name.lowercased().contains(searchText.lowercased())
        })
        collectionView?.reloadData()
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "resultsVC") as! ResultsTableViewController
        viewController.mItem = filteredItems[indexPath.item]
        navigationController?.pushViewController(viewController, animated: true)
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
