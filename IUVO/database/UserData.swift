//
//  UserData.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 28/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers class UserData: Object {
    
    dynamic var key : String?
    dynamic var firstName : String?
    dynamic var lastName : String?
    dynamic var fullNmae : String?
    dynamic var email : String?
    dynamic var phone : String?
    dynamic var isGeneralUser : Bool = false
    dynamic var verified : Bool = false
    dynamic var meansOfID : String?
    dynamic var idNumber : String?
    dynamic var needsUpload : Bool = false
    dynamic var declineMessage : String?
    dynamic var profilePic : String?
    
    func toDict() -> [String : Any] {
        return ["key" : key ?? "",
                "firstName" : firstName ?? "",
                "lastName" : lastName ?? "",
                "fullNmae" : fullNmae ?? "",
                "email" : email ?? "",
                "phone" : phone ?? "",
                "generalUser" : isGeneralUser,
                "verified" : verified,
                "meansOfID" : meansOfID ?? "",
                "idNumber" : idNumber ?? "",
                "needsUpload" : needsUpload,
                "declineMessage" : declineMessage ?? "",
                "profilePic" : profilePic ?? ""]
    }
    
}
