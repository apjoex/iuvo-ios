//
//  RealmHelper.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 28/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import Foundation
import RealmSwift


class RealmHelper {
    
    private init() {}
    static let shared = RealmHelper()
    
    var realm = try! Realm()
    
    func create<T:Object>(_ object : T) {
        do {
            try realm.write {
                realm.add(object)
            }
        }catch{
            
        }
    }
    
    func clearDatabase() {
        do {
            try realm.write {
                realm.deleteAll()
            }
        } catch {
            
        }
    }
    
    func getUser() -> UserData? {
        return realm.objects(UserData.self).first
    }
    
}
