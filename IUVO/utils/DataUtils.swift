//
//  DataUtils.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 13/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import Foundation
import UIKit

class DataUtils {
    
    static let shared = DataUtils()
    
    let services = [
        Item(name: "Petrol Stations", type: "gas_station", img: #imageLiteral(resourceName: "petrol_station"), isFromGoogle: true),
        Item(name: "Lawyer", type: "lawyer", img: #imageLiteral(resourceName: "lawyers"), isFromGoogle: true),
        Item(name: "Publishers", type: "book_store", img: #imageLiteral(resourceName: "publishers"), isFromGoogle: true),
        Item(name: "Banks", type: "bank", img: #imageLiteral(resourceName: "banks"), isFromGoogle: true),
        Item(name: "ATMs", type: "atm", img: #imageLiteral(resourceName: "atm"), isFromGoogle: true),
        Item(name: "Schools", type: "school", img: #imageLiteral(resourceName: "school"), isFromGoogle: true),
        Item(name: "Police posts", type: "police", img: #imageLiteral(resourceName: "police_posts"), isFromGoogle: true),
        Item(name: "Hospitals", type: "hospital", img: #imageLiteral(resourceName: "hospital"), isFromGoogle: true),
        Item(name: "Hotels", type: "lodging", img: #imageLiteral(resourceName: "hotels"), isFromGoogle: true),
        Item(name: "Pharmacy", type: "pharmacy", img: #imageLiteral(resourceName: "pharmacy"), isFromGoogle: true),
        Item(name: "Relaxation centers", type: "amusement_park", img: #imageLiteral(resourceName: "relaxation"), isFromGoogle: true),
        Item(name: "Shopping malls", type: "shopping_mall", img: #imageLiteral(resourceName: "shopping_mall"), isFromGoogle: true),
        Item(name: "Gym/Fitness/Spa", type: "spa", img: #imageLiteral(resourceName: "gym"), isFromGoogle: false),
        Item(name: "Art studios", type: "art_gallery", img: #imageLiteral(resourceName: "art_studio"), isFromGoogle: true),
        Item(name: "Hair salons", type: "beauty_salon", img: #imageLiteral(resourceName: "hair_salon"), isFromGoogle: false),
        Item(name: "Laundry", type: "laundry", img: #imageLiteral(resourceName: "laundry"), isFromGoogle: false),
        Item(name: "Bars and Cuisines", type: "bar", img: #imageLiteral(resourceName: "bar"), isFromGoogle: true),
        Item(name: "Bakery", type: "bakery", img: #imageLiteral(resourceName: "bakery"), isFromGoogle: true),
        Item(name: "Casino", type: "casino", img: #imageLiteral(resourceName: "casino"), isFromGoogle: true),
        Item(name: "Cafe", type: "cafe", img: #imageLiteral(resourceName: "cafe"), isFromGoogle: true),
        Item(name: "Car wash", type: "car_wash", img: #imageLiteral(resourceName: "car_wash"), isFromGoogle: true),
        Item(name: "Court house", type: "courthouse", img: #imageLiteral(resourceName: "court_house"), isFromGoogle: true),
        Item(name: "Dentist", type: "dentist", img: #imageLiteral(resourceName: "dentist"), isFromGoogle: false),
        Item(name: "Electrician", type: "electrician", img: #imageLiteral(resourceName: "electrician"), isFromGoogle: false),
        Item(name: "Embassy", type: "embassy", img: #imageLiteral(resourceName: "embassy"), isFromGoogle: true),
        Item(name: "Fire station", type: "fire_station", img: #imageLiteral(resourceName: "fire_station"), isFromGoogle: true),
        Item(name: "Florist", type: "florist", img: #imageLiteral(resourceName: "florist"), isFromGoogle: true),
        Item(name: "Insurance agency", type: "insurance_agency", img: #imageLiteral(resourceName: "insurance"), isFromGoogle: true),
        Item(name: "Library", type: "library", img: #imageLiteral(resourceName: "library"), isFromGoogle: true),
        Item(name: "Movie theater", type: "movie_theater", img: #imageLiteral(resourceName: "movie_theater"), isFromGoogle: true),
        Item(name: "Musuem", type: "museum", img: #imageLiteral(resourceName: "museum"), isFromGoogle: true),
        Item(name: "Night club", type: "night_club", img: #imageLiteral(resourceName: "night_club"), isFromGoogle: true),
        Item(name: "Painter", type: "painter", img: #imageLiteral(resourceName: "painter"), isFromGoogle: false),
        Item(name: "Park", type: "park", img: #imageLiteral(resourceName: "park"), isFromGoogle: true),
        Item(name: "Post office", type: "post_office", img: #imageLiteral(resourceName: "post_office"), isFromGoogle: true),
        Item(name: "Real estate agency", type: "real_estate_agency", img: #imageLiteral(resourceName: "real_estate"), isFromGoogle: true),
        Item(name: "Restaurant", type: "restaurant", img: #imageLiteral(resourceName: "restaurant"), isFromGoogle: true),
        Item(name: "Stadium", type: "stadium", img: #imageLiteral(resourceName: "statdium"), isFromGoogle: true),
        Item(name: "Travel agency", type: "travel_agency", img: #imageLiteral(resourceName: "travel_agency"), isFromGoogle: false),
        Item(name: "Veterinary care", type: "veterinary_care", img: #imageLiteral(resourceName: "vet"), isFromGoogle: false),
        Item(name: "Zoo", type: "zoo", img: #imageLiteral(resourceName: "zoo"), isFromGoogle: true),
        Item(name: "Airport", type: "airport", img:#imageLiteral(resourceName: "airport"), isFromGoogle: true)
    ]
    
    
    let repairs = [
        Item(name: "Automobile", type: "", img: #imageLiteral(resourceName: "automobiles"), isFromGoogle: false),
        Item(name: "Home electronices", type: "", img: #imageLiteral(resourceName: "home_electronics"), isFromGoogle: false),
        Item(name: "Petrol engines", type: "", img: #imageLiteral(resourceName: "petrol_engines"), isFromGoogle: false),
        Item(name: "Diesel engines", type: "", img: #imageLiteral(resourceName: "diesel_engines"), isFromGoogle: false),
        Item(name: "High voltage electronics", type: "", img: #imageLiteral(resourceName: "high_voltage"), isFromGoogle: false),
        Item(name: "Mobile gadgets", type: "", img: #imageLiteral(resourceName: "mobile_gadgets"), isFromGoogle: false),
        Item(name: "Phones and computers", type: "", img: #imageLiteral(resourceName: "phones_computers"), isFromGoogle: false),
        Item(name: "Plumber", type: "", img: #imageLiteral(resourceName: "plumber"), isFromGoogle: false),
        Item(name: "Locksmith", type: "", img: #imageLiteral(resourceName: "locksmith"), isFromGoogle: false)
    ]
    
    let sales = [
         Item(name: "Auto spare parts", type: "", img: #imageLiteral(resourceName: "auto_spare_parts"), isFromGoogle: false),
         Item(name: "Household fittings", type: "", img: #imageLiteral(resourceName: "household_fittings"), isFromGoogle: false),
         Item(name: "Home appliances", type: "", img: #imageLiteral(resourceName: "home_appliances"), isFromGoogle: false),
         Item(name: "Phone and accessories", type: "", img: #imageLiteral(resourceName: "phone_accessories"), isFromGoogle: false),
         Item(name: "Electronic gadgets", type: "", img: #imageLiteral(resourceName: "electronic_gadgets"), isFromGoogle: false),
         Item(name: "Automobiles", type: "", img: #imageLiteral(resourceName: "automobile_sales"), isFromGoogle: false),
         Item(name: "Farm products", type: "", img: #imageLiteral(resourceName: "farm_products"), isFromGoogle: false),
         Item(name: "Cakes and groceries", type: "", img: #imageLiteral(resourceName: "cales_groceries"), isFromGoogle: false),
         Item(name: "Food and consumables", type: "", img: #imageLiteral(resourceName: "food_consumables"), isFromGoogle: false),
         Item(name: "Boutique and fashion houses", type: "", img: #imageLiteral(resourceName: "boutique"), isFromGoogle: false),
         Item(name: "Household/Office furniture", type: "", img: #imageLiteral(resourceName: "household_furniture"), isFromGoogle: false),
         Item(name: "Bookstore", type: "", img: #imageLiteral(resourceName: "bookstore"), isFromGoogle: false),
         Item(name: "Liquor store", type: "", img: #imageLiteral(resourceName: "liquor"), isFromGoogle: false),
         Item(name: "Jewelry store", type: "", img: #imageLiteral(resourceName: "jewelry"), isFromGoogle: false),
         Item(name: "Pet store", type: "", img: #imageLiteral(resourceName: "pet_store"), isFromGoogle: false),
         Item(name: "Shoe store", type: "", img: #imageLiteral(resourceName: "shoe_store"), isFromGoogle: false)         
    ]
    
    var mPlace : Result?
    var mItem : Item!
    var mDistance : Double!
    
}

struct Item {
    var name : String
    var type : String
    var img : UIImage
    var isFromGoogle : Bool
}
