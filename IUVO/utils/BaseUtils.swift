//
//  BaseUtils.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 13/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import Foundation
import UIKit
import FirebaseStorage

class BaseUtils {
    
    static let shared = BaseUtils()
    
    let MINIMUM_CHARACTER_LENGTH = 8
    let defaultErrorMsg = "Something went wrong"
    let API_KEY = "AIzaSyB04yK9KIMtjOxRnx_8Mt5EK_fhUli28Tw"
    let radius = 1200
    
    var isLoggedIn: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "isLoggedIn")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "isLoggedIn")
        }
    }
    
    var onBoardingShown: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "onBoardingShown")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "onBoardingShown")
        }
    }
    
    var isCustomLocationSet: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "isCustomLocationSet")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "isCustomLocationSet")
        }
    }
    
    var customLat: Double {
        get {
            return UserDefaults.standard.double(forKey: "customLat")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "customLat")
        }
    }
    
    var customlong: Double {
        get {
            return UserDefaults.standard.double(forKey: "customlong")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "customlong")
        }
    }
    
    var customAddress: String {
        get {
            return UserDefaults.standard.string(forKey: "customAddress") ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "customAddress")
        }
    }
    
    func toUserDataClass(data : [String : Any]) -> UserData {
        let mUser = UserData()
        mUser.key = data["key"] as? String ?? ""
        mUser.firstName = data["firstName"] as? String ?? ""
        mUser.lastName = data["lastName"] as? String ?? ""
        mUser.fullNmae = data["fullNmae"] as? String ?? ""
        mUser.email = data["email"] as? String ?? ""
        mUser.phone = data["phone"] as? String ?? ""
        mUser.isGeneralUser = data["generalUser"] as? Bool ?? false
        mUser.verified = data["verified"] as? Bool ?? false
        mUser.meansOfID = data["meansOfID"] as? String ?? ""
        mUser.idNumber = data["idNumber"] as? String ?? ""
        mUser.needsUpload = data["needsUpload"] as? Bool ?? false
        mUser.declineMessage = data["declineMessage"] as? String ?? ""
        mUser.profilePic = data["profilePic"] as? String ?? ""
        return mUser
    }
    
    func makeFirebaseFriendlyPath(name : String) -> String {
        var newPath = name.replacingOccurrences(of: " ", with: "_")
        newPath = newPath.replacingOccurrences(of: "/", with: " ")
        return newPath.lowercased()
    }
        
}
