//
//  Extensions.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 06/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import Foundation
import UIKit
import JGProgressHUD

private var __maxLengths = [UITextField: Int]()

extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
        guard let t = textField.text else {return}
        textField.text = String(t.characters.prefix(maxLength))
    }
}

extension String {
    
    var isValidEmail : Bool {
        get{
            let regEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
            let pred = NSPredicate(format:"SELF MATCHES %@", regEx)
            return pred.evaluate(with: self)
        }
    }
}

extension NSMutableAttributedString {
    
    func setColor(color: UIColor, forText stringValue: String) {
        let range: NSRange = self.mutableString.range(of: stringValue, options: .caseInsensitive)
        self.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: range)
    }
    
}

extension UIViewController{
    
    func showPrompt(message : String) {
        let alertVC = UIAlertController(title: message, message: nil, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Okay", style: UIAlertAction.Style.default) { (_) in
            alertVC.dismiss(animated: true, completion: nil)
        }
        alertVC.addAction(okAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func showPromptAndTakeAction(message : String, action: @escaping () -> Void) {
        let alertVC = UIAlertController(title: message, message: nil, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Okay", style: UIAlertAction.Style.default) { (_) in
            alertVC.dismiss(animated: true, completion: nil)
            action()
        }
        alertVC.addAction(okAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func showPromptAndFinish(message : String) {
        let alertVC = UIAlertController(title: message, message: nil, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Okay", style: UIAlertAction.Style.default) { (_) in
            alertVC.dismiss(animated: true, completion: nil)
            //self.dismiss(animated: true, completion: nil)
            self.navigationController?.popViewController(animated: true)
        }
        alertVC.addAction(okAction)
        self.present(alertVC, animated: true, completion: nil)
    }
    
    func showHUD(message : String) -> JGProgressHUD {
        let hud = JGProgressHUD(style: .light)
        hud.textLabel.text = message
        hud.interactionType = .blockAllTouches
        hud.show(in: self.view)
        return hud
    }
    
    func dismissHUD(hud : JGProgressHUD) {
        hud.dismiss()
    }
    
    func logOutAndWipeData() {
        
        do {
            try RealmHelper.shared.realm.write {
                RealmHelper.shared.realm.deleteAll()
            }
        }catch{
            print("Unable to delete Realm")
        }
        
        let domain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: domain)
        
        let startVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "start") as! StartViewController
        present(startVC, animated: true, completion: nil)
    }
    
}

extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()
        
        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        
        return result
    }
}

extension UICollectionView {
    
    func scrollToLast() {
        guard numberOfSections > 0 else {
            return
        }
        
        let lastSection = numberOfSections - 1
        
        guard numberOfItems(inSection: lastSection) > 0 else {
            return
        }
        
        let lastItemIndexPath = IndexPath(item: numberOfItems(inSection: lastSection) - 1,
                                          section: lastSection)
        scrollToItem(at: lastItemIndexPath, at: .bottom, animated: true)
    }
}
