//
//  CustomFlowLayout.swift
//  IUVO
//
//  Created by AKINDE-PETERS on 13/08/2018.
//  Copyright © 2018 AKINDE-PETERS. All rights reserved.
//

import UIKit

class CustomFlowLayout: UICollectionViewFlowLayout {
    
    override init() {
        super.init()
        setupLayout()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupLayout()
    }
    
    func setupLayout() {
        minimumInteritemSpacing = 10
        minimumLineSpacing = 10
        scrollDirection = .vertical
        sectionInset = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
    override var itemSize: CGSize{
        set{
            
        }
        
        get{
            let numberOfColumns: CGFloat = 3
            let itemWidth = (self.collectionView!.frame.width - 45) / numberOfColumns
            return CGSize(width: itemWidth, height: itemWidth)
        }
    }
    
}
