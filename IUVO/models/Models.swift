//
//  Models.swift
//  IUVO
//
//  Created by JOSEPH AKINDE-PETERS on 14/01/2019.
//  Copyright © 2019 AKINDE-PETERS. All rights reserved.
//

import Foundation
import MessageKit

struct Location : Decodable {
    let lat : Double
    let lng : Double
}

struct Geometry : Decodable {
    let location : Location?
}

struct Photo : Decodable {
    let photo_reference : String
}

struct OpeningHour : Decodable {
    let open_now : Bool
}

struct Result : Decodable {
    let geometry : Geometry?
    let icon : String
    let id : String
    let name : String
    let opening_hours : OpeningHour?
    let photos : [Photo]?
    let place_id : String
    let rating : Float?
    let reference : String
    let vicinity : String
    let mail : String?
    let phone : String?
    let website : String?
    let customSource : Bool?
    let ownerID : String?
    let ownerName : String?
    let category : String?
    
    init(dict : [String : Any]) {
        
        let geoDict = dict["geometry"] as! [String : Any]
        let locDict = geoDict["location"] as! [String : Any]
        let lat = locDict["lat"] as! Double
        let lng = locDict["lng"] as! Double
        
        self.geometry = Geometry(location: Location(lat: lat, lng: lng))
        self.icon = dict["icon"] as! String
        self.id = dict["id"] as! String
        self.name = dict["name"] as! String
        self.opening_hours = OpeningHour(open_now: true)
        self.photos = nil
        self.place_id = dict["place_id"] as! String
        self.rating = 0
        self.reference = dict["reference"] as! String
        self.vicinity = dict["vicinity"] as! String
        self.mail = dict["mail"] as? String
        self.phone = dict["phone"] as? String
        self.website = dict["website"] as? String
        self.customSource = dict["customSource"] as? Bool
        self.ownerID = dict["ownerID"] as? String
        self.ownerName = dict["ownerName"] as? String
        self.category = dict["category"] as? String
        
    }
}

struct WebsiteData : Decodable {
    let results : [Result]?
}


struct FavData : Decodable {
    let result : Result?
}

struct Message : MessageType{
    var sender: Sender
    var messageId: String
    var sentDate: Date
    var kind: MessageKind
}
